/*
License
	Copyright (C) 2016 Peter Cheung (mcheung63@hotmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.github.netbeanssyncsettings;

import com.peterswing.CommonLib;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

public class ModuleLib {

	public static boolean isDebug = true;

	public static void log(String str) {
		if (isDebug) {
			InputOutput io = IOProvider.getDefault().getIO("sync settings", false);
			io.getOut().println(str);
		}
	}

	public static void log(Object obj) {
		log(obj.toString());
	}

	public static void zip(final Path folder, final Path zipFilePath) throws IOException {
		try (FileOutputStream fos = new FileOutputStream(zipFilePath.toFile());
				ZipOutputStream zos = new ZipOutputStream(fos)) {
			Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					zos.putNextEntry(new ZipEntry(folder.relativize(file).toString()));
					Files.copy(file, zos);
					zos.closeEntry();
					return FileVisitResult.CONTINUE;
				}

				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					zos.putNextEntry(new ZipEntry(folder.relativize(dir).toString() + "/"));
					zos.closeEntry();
					return FileVisitResult.CONTINUE;
				}
			});
		}
	}

	public static void unzip(String zipFilePath, String destDir) {
		File dir = new File(destDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		FileInputStream fis;
		byte[] buffer = new byte[1024];
		try {
			fis = new FileInputStream(zipFilePath);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				//ModuleLib.log("\t\t\t" + destDir + File.separator + fileName);

				File newFile = new File(destDir + File.separator + fileName);
				if (!ze.isDirectory()) {
					newFile.getParentFile().mkdirs();
					new File(newFile.getParent()).mkdirs();
					try {
						FileOutputStream fos = new FileOutputStream(newFile);
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
						fos.close();
					} catch (Exception ex) {
						ModuleLib.log(CommonLib.printException(ex));
					}
					zis.closeEntry();
				} else {
					if (!newFile.exists()) {
						newFile.mkdirs();
						ModuleLib.log("creating folder " + newFile.getAbsolutePath());
					}
				}
				ze = zis.getNextEntry();
			}
			//close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();
		} catch (IOException ex) {
			ModuleLib.log(CommonLib.printException(ex));
		}

	}
}
