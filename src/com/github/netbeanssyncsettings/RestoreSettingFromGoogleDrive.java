/*
License
	Copyright (C) 2016 Peter Cheung (mcheung63@hotmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.github.netbeanssyncsettings;

import static com.github.netbeanssyncsettings.SyncSettingsToGoogleDrive.getDriveService;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import com.peterswing.CommonLib;
import com.peterswing.advancedswing.jprogressbardialog.JProgressBarDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Window",
		id = "com.github.netbeanssyncsettings.RestoreSettingFromGoogleDrive"
)
@ActionRegistration(
		displayName = "#CTL_RestoreSettingFromGoogleDrive",
		iconBase = "com/github/netbeanssyncsettings/download.png"
)
@ActionReference(path = "Menu/Window/Sync setting", position = 2)
@Messages("CTL_RestoreSettingFromGoogleDrive=Restore setting from google drive")
public final class RestoreSettingFromGoogleDrive implements ActionListener {

	static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm:ss");

	@Override
	public void actionPerformed(ActionEvent e) {
		final JProgressBarDialog d = new JProgressBarDialog("Please wait", true);

		d.progressBar.setIndeterminate(true);
		d.progressBar.setStringPainted(true);
		d.progressBar.setString("Restoring settings from google drive");
		Thread longRunningThread = new Thread() {
			public void run() {
				try {
					ModuleLib.log("Restoring setting from google drive");
					Drive service = getDriveService();

					String folderId = null;
					String pageToken = null;
					ModuleLib.log("\tfinding folder 'netbeans toolbar sync'");
					do {
						FileList result = service.files().list()
								.setQ("name='netbeans toolbar sync' and trashed=false and mimeType = 'application/vnd.google-apps.folder'")
								.setSpaces("drive")
								.setFields("nextPageToken, files(id, name)")
								.setPageToken(pageToken)
								.execute();
						List<com.google.api.services.drive.model.File> files = result.getFiles();
						if (files.size() > 0) {
							ModuleLib.log("\tfound folder, name=" + files.get(0).getName());
							folderId = files.get(0).getId();
							break;
						}
						pageToken = result.getNextPageToken();
					} while (pageToken != null);

					pageToken = null;
					ModuleLib.log("\tfinding file " + SyncSettingsToGoogleDrive.zipFilename);
					com.google.api.services.drive.model.File file = null;
					do {
						FileList result = service.files().list()
								.setQ("'" + folderId + "' in parents and name='" + SyncSettingsToGoogleDrive.zipFilename + "' and trashed=false")
								.setSpaces("drive")
								.setFields("nextPageToken, files(id, name, modifiedTime)")
								.setOrderBy("modifiedTime desc")
								.setPageToken(pageToken)
								.execute();
						List<com.google.api.services.drive.model.File> files = result.getFiles();
						if (files.size() > 0) {
							file = files.get(0);
							break;
						}
						pageToken = result.getNextPageToken();
					} while (pageToken != null);

					if (file == null) {
						ModuleLib.log("\tcan't find " + SyncSettingsToGoogleDrive.zipFilename);
					} else {
						ModuleLib.log("\tfound " + file.getName() + ", " + simpleDateFormat.format(new Date(file.getModifiedTime().getValue())));
						File targetFile = new File(SyncSettingsToGoogleDrive.toolbarParentFolder + File.separator + SyncSettingsToGoogleDrive.zipFilename);
						ModuleLib.log("\tzip is saved to " + targetFile.getAbsolutePath());
						if (!targetFile.exists()) {
							targetFile.createNewFile();
						}
						FileOutputStream outputStream = new FileOutputStream(targetFile);
						service.files().get(file.getId()).executeMediaAndDownloadTo(outputStream);

						ModuleLib.log("\tunzipping " + targetFile.getAbsolutePath());
						CommonLib.deleteDirectory(new File(SyncSettingsToGoogleDrive.toolbarFolder));
						new File(SyncSettingsToGoogleDrive.toolbarFolder).mkdirs();
						ModuleLib.unzip(targetFile.getAbsolutePath(), SyncSettingsToGoogleDrive.toolbarFolder);
						targetFile.delete();
					}
					ModuleLib.log("\t\tdone");
				} catch (Exception ex) {
					ModuleLib.log(CommonLib.printException(ex));
				}
			}
		};
		d.thread = longRunningThread;
		d.setVisible(true);
	}
}
