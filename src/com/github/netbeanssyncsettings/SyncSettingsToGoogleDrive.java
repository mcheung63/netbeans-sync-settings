/*
License
	Copyright (C) 2016 Peter Cheung (mcheung63@hotmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.github.netbeanssyncsettings;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;

import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import com.peterswing.CommonLib;
import com.peterswing.advancedswing.jprogressbardialog.JProgressBarDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Window",
		id = "com.github.netbeanssyncsettings.SyncSettingsToGoogleDrive"
)
@ActionRegistration(
		displayName = "#CTL_SyncSettingsToGoogleDrive",
		iconBase = "com/github/netbeanssyncsettings/upload.png"
)
@ActionReference(path = "Menu/Window/Sync setting", position = 1)
@Messages("CTL_SyncSettingsToGoogleDrive=Sync settings to google drive")
public final class SyncSettingsToGoogleDrive implements ActionListener {

	private static final String APPLICATION_NAME = "netbeans toolbar sync";
	private static final File DATA_STORE_DIR = new File(System.getProperty("user.home"), ".credentials/netbeans-toolbar-sync");
	private static FileDataStoreFactory DATA_STORE_FACTORY;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static HttpTransport HTTP_TRANSPORT;
	private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE);
	public static String toolbarParentFolder = System.getProperty("netbeans.user");
	public static String toolbarFolder = toolbarParentFolder + File.separator + "config";
	public static String zipFilename = "config.zip";

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public static Credential authorize() throws IOException {
		// Load client secrets.
		InputStream in = SyncSettingsToGoogleDrive.class.getResourceAsStream("/com/github/netbeanssyncsettings/client_secret.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();
		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		ModuleLib.log("\tCredentials saved to " + DATA_STORE_DIR.getAbsolutePath());
		return credential;
	}

	public static Drive getDriveService() throws IOException {
		Credential credential = authorize();
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final JProgressBarDialog d = new JProgressBarDialog("Please wait", true);

		d.progressBar.setIndeterminate(true);
		d.progressBar.setStringPainted(true);
		d.progressBar.setString("Uploading settings to google drive");

		Thread longRunningThread = new Thread() {
			public void run() {
				try {
					ModuleLib.log("zipping " + new File(toolbarFolder).getAbsolutePath());
					File temp = Files.createTempDirectory("netbeansSyncSettings").toFile();
					String outputPath = temp + File.separator + zipFilename;
					File outputFile = new File(outputPath);
					ModuleLib.log("\t" + outputPath);
					ModuleLib.zip(new File(toolbarFolder).toPath(), outputFile.toPath());

					d.progressBar.setString("Connecting to google drive");
					Drive service = getDriveService();
					ModuleLib.log("\tConnecting to google drive");

					String folderId = null;
					String pageToken = null;
					do {
						FileList result = service.files().list()
								.setQ("name='netbeans toolbar sync' and trashed=false")
								.setSpaces("drive")
								.setFields("nextPageToken, files(id, name)")
								.setPageToken(pageToken)
								.execute();
						List<com.google.api.services.drive.model.File> files = result.getFiles();
						if (files.size() > 0) {
							ModuleLib.log("\tfound folder, name=" + files.get(0).getName());
							folderId = files.get(0).getId();
							break;
						}
						pageToken = result.getNextPageToken();
					} while (pageToken != null);

					d.progressBar.setString("Folder ID=" + folderId);
					ModuleLib.log("\tfolderId=" + folderId);

					if (folderId == null) {
						d.progressBar.setString("Creating folder : netbeans toolbar sync");
						com.google.api.services.drive.model.File folderMetadata = new com.google.api.services.drive.model.File();
						folderMetadata.setName("netbeans toolbar sync");
						folderMetadata.setMimeType("application/vnd.google-apps.folder");
						com.google.api.services.drive.model.File newFolder = service.files().create(folderMetadata).setFields("id").execute();
						folderId = newFolder.getId();
					}

					ModuleLib.log("\t\tUploading " + zipFilename + ", size = " + outputFile.length());
					d.progressBar.setString("Uploading " + zipFilename + ", size = " + outputFile.length());
					com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
					fileMetadata.setName(outputFile.getName());
					fileMetadata.setParents(Collections.singletonList(folderId));
					FileContent mediaContent = new FileContent("application/xml", outputFile);
					com.google.api.services.drive.model.File uploadedFile = service.files().create(fileMetadata, mediaContent).setFields("id, parents").execute();

					ModuleLib.log("\t\t\tdone");
					d.progressBar.setString("Done");

					outputFile.delete();
				} catch (Exception ex) {
					ModuleLib.log(CommonLib.printException(ex));
				}
			}
		};
		d.thread = longRunningThread;
		d.setVisible(true);
	}

}
