# netbeans-sync-settings
Sync netbeans settings to google drive

![alt tag](https://github.com/mcheung63/netbeans-sync-settings/raw/master/logo.png)

# Compile

In netbeans, right click project -> properties -> libraries, add those jars (libInUse/lib) into your project

# To sign the nbm
	
Step 1) Copy keystore to NetBeansProjects/netbeans-sync-settings/nbproject/private/keystore

Step 2) Just build the nbm
	
# License
	Copyright (C) 2016 Peter Cheung (mcheung63@hotmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.